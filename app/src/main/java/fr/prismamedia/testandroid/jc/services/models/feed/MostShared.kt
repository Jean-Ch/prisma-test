package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class MostShared (

  @SerializedName("date"  ) var date  : String? = null,
  @SerializedName("count" ) var count : Int?    = null

)