package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Data (

  @SerializedName("date"             ) var date             : String?          = null,
  @SerializedName("currentItemCount" ) var currentItemCount : Int?             = null,
  @SerializedName("totalItems"       ) var totalItems       : Int?             = null,
  @SerializedName("startIndex"       ) var startIndex       : Int?             = null,
  @SerializedName("itemsPerPage"     ) var itemsPerPage     : Int?             = null,
  @SerializedName("items"            ) var items            : ArrayList<Items> = arrayListOf()

)