package fr.prismamedia.testandroid.jc.ui.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.prismamedia.testandroid.jc.R
import fr.prismamedia.testandroid.jc.databinding.AdapterFeedBinding
import fr.prismamedia.testandroid.jc.services.models.feed.Feed
import fr.prismamedia.testandroid.jc.services.models.feed.Items
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import kotlin.collections.ArrayList

class FeedAdapter(var listener: (fav: Items) -> Unit = {}, val displayOnlyFav: Boolean = false) :
    RecyclerView.Adapter<FeedViewHolder>() {

    var feed: Feed = Feed()
    var favItems: List<String> = listOf()

    fun setFeeds(feed: Feed) {
        this.feed = feed
        if (displayOnlyFav) {
            this.feed = feed.copy(
                feed.apiVersion,
                feed.data?.copy(
                    feed.data?.date,
                    feed.data?.currentItemCount,
                    feed.data?.totalItems,
                    feed.data?.startIndex,
                    feed.data?.itemsPerPage,
                    feed.data?.items?.filter { favItems.contains(it.resource?.id) } as ArrayList<Items>
                )
            )
        } else {
            this.feed = feed
        }
        notifyDataSetChanged()
    }

    fun setFavItem(idList: List<String>) {
        this.favItems = idList
        if (displayOnlyFav) {
            setFeeds(this.feed)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterFeedBinding.inflate(inflater, parent, false)
        return FeedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {
        val item = feed.data?.items?.let { it[position] }
        holder.binding.title.text = item?.title
        val drawable = if (favItems.contains(item?.resource?.id)) {
            R.drawable.ic_star
        } else {
            R.drawable.ic_baseline_star_border_24
        }
        holder.binding.favIcon.setImageDrawable(
            ContextCompat.getDrawable(
                holder.itemView.context,
                drawable
            )
        )
        item?.medias?.images?.firstOrNull { !it.original?.url.isNullOrBlank() }?.original?.url?.let { imgUrl ->
            Glide.with(holder.itemView.context).load(imgUrl).into(holder.binding.imageview)
        } ?: run {
            holder.binding.imageview.setImageDrawable(null)
        }

        holder.binding.date.text = DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ).withLocale( Locale.getDefault() ).format(
            LocalDateTime.parse(item?.published, DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.getDefault() )))
        item?.let { i -> holder.binding.favIcon.setOnClickListener { listener(i) } }
    }

    override fun getItemCount(): Int {
        return if (displayOnlyFav) {
            feed.data?.items?.count() ?: 0
        } else {
            feed.data?.items?.count() ?: 0
        }
    }

}

class FeedViewHolder(val binding: AdapterFeedBinding) : RecyclerView.ViewHolder(binding.root)
