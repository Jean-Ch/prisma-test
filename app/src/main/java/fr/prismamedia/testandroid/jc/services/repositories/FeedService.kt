package fr.prismamedia.testandroid.jc.services.repositories

import fr.prismamedia.testandroid.jc.services.models.feed.Feed
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface FeedService {

    @GET("julienbanse/34cdfbd1c094b2dddffce2b5d5533d6b/raw/15b5f322838e08bf8a38985b7aa94f6c758d6741/news.json")
    suspend fun getFeed() : Response<Feed>

    companion object {
        var retrofitService: FeedService? = null
        fun getInstance() : FeedService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://gist.githubusercontent.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(FeedService::class.java)
            }
            return retrofitService!!
        }
    }

}