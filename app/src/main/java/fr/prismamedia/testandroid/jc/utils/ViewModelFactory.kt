package fr.prismamedia.testandroid.jc.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.prismamedia.testandroid.jc.services.repositories.MainRepository
import fr.prismamedia.testandroid.jc.ui.viewmodels.FeedViewModel

class ViewModelFactory constructor(private val repository: MainRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(FeedViewModel::class.java)) {
            FeedViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}