package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Items (

  @SerializedName("mostViewed"                ) var mostViewed                : MostViewed?       = MostViewed(),
  @SerializedName("resource"                  ) var resource                  : Resource?         = Resource(),
  @SerializedName("published"                 ) var published                 : String?           = null,
  @SerializedName("body"                      ) var body                      : String?           = null,
  @SerializedName("title"                     ) var title                     : String?           = null,
  @SerializedName("dateIndexed"               ) var dateIndexed               : String?           = null,
  @SerializedName("universalUniqueIdentifier" ) var universalUniqueIdentifier : String?           = null,
  @SerializedName("lead"                      ) var lead                      : String?           = null,
  @SerializedName("medias"                    ) var medias                    : Medias?           = Medias(),
  @SerializedName("path"                      ) var path                      : ArrayList<String> = arrayListOf(),
  @SerializedName("bodySize"                  ) var bodySize                  : Int?              = null,
  @SerializedName("category"                  ) var category                  : Category?         = Category(),
  @SerializedName("contentTypes"              ) var contentTypes              : ArrayList<String> = arrayListOf(),
  @SerializedName("mostShared"                ) var mostShared                : MostShared?       = MostShared(),
  @SerializedName("authors"                   ) var authors                   : ArrayList<Authors> = arrayListOf(),
  @SerializedName("keywords"                  ) var keywords                  : ArrayList<String> = arrayListOf()

)