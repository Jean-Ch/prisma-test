package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Images (

  @SerializedName("copyright" ) var copyright : String?   = null,
  @SerializedName("original"  ) var original  : Original? = Original()

)