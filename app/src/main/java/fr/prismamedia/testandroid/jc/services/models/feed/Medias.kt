package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Medias (

  @SerializedName("imageCount" ) var imageCount : Int?              = null,
  @SerializedName("videoCount" ) var videoCount : Int?              = null,
  @SerializedName("images"     ) var images     : ArrayList<Images> = arrayListOf(),
  @SerializedName("videos"     ) var videos     : String?           = null,
  @SerializedName("podcast"    ) var podcast    : String?           = null

)