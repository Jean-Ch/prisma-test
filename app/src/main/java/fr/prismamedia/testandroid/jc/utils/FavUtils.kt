package fr.prismamedia.testandroid.jc.utils

import android.content.Context
import android.content.SharedPreferences

class FavUtils {

    companion object {
        private const val SHARED_FILE = "file"
        fun handleFavorite(context: Context, id: String?): Boolean {
            if(id == null) {
                return false
            }

            val sharedPreferences: SharedPreferences = context.getSharedPreferences(SHARED_FILE, Context.MODE_PRIVATE)
            val isFaved = sharedPreferences.getBoolean(id, false)
            sharedPreferences.edit().putBoolean(id, !isFaved).apply()
            return !isFaved
        }

        fun getAllFav(context: Context): List<String> {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences(SHARED_FILE, Context.MODE_PRIVATE)
            return sharedPreferences.all.filter { it.value == true }.map {it.key}
        }
    }

}