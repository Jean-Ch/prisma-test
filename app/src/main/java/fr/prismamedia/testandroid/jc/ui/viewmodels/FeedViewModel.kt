package fr.prismamedia.testandroid.jc.ui.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.prismamedia.testandroid.jc.services.models.feed.Feed
import fr.prismamedia.testandroid.jc.services.models.feed.Items
import fr.prismamedia.testandroid.jc.services.repositories.MainRepository
import fr.prismamedia.testandroid.jc.utils.FavUtils
import kotlinx.coroutines.*

class FeedViewModel constructor(private val mainRepository: MainRepository) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val feed = MutableLiveData<Feed>()
    val favFeed = MutableLiveData<MutableList<String>>()
    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val loading = MutableLiveData<Boolean>()

    fun getFeed(context: Context) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getFeed()
            withContext(Dispatchers.Main) {
                favFeed.postValue(FavUtils.getAllFav(context).toMutableList())
                if (response.isSuccessful) {
                    feed.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

    fun updateItem(fav: Items, newFavState: Boolean) {
        fav.resource?.id?.let {
            id ->
            val index = favFeed.value?.indexOf(id)
            if(index == -1) {
                favFeed.value?.add(id)
            } else {
                favFeed.value?.remove(id)
            }
            favFeed.value = favFeed.value
        }
    }

}
