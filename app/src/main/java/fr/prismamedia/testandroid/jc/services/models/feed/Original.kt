package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Original (

  @SerializedName("url" ) var url : String? = null

)