package fr.prismamedia.testandroid.jc.ui.favorites

import fr.prismamedia.testandroid.jc.ui.news.FeedAdapter
import fr.prismamedia.testandroid.jc.ui.news.HomeFragment

class FavFragment : HomeFragment() {
    override val adapter = FeedAdapter(displayOnlyFav = true)
}