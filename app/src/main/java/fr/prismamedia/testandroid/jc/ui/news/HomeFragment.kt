package fr.prismamedia.testandroid.jc.ui.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import fr.prismamedia.testandroid.jc.databinding.FragmentHomeBinding
import fr.prismamedia.testandroid.jc.services.models.feed.Items
import fr.prismamedia.testandroid.jc.services.repositories.FeedService
import fr.prismamedia.testandroid.jc.services.repositories.MainRepository
import fr.prismamedia.testandroid.jc.ui.viewmodels.FeedViewModel
import fr.prismamedia.testandroid.jc.utils.FavUtils
import fr.prismamedia.testandroid.jc.utils.ViewModelFactory

open class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    protected open val adapter = FeedAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val retrofitService = FeedService.getInstance()
        val mainRepository = MainRepository(retrofitService)
        val viewModel =
            ViewModelProvider(this, ViewModelFactory(mainRepository))[FeedViewModel::class.java]
        binding.recyclerview.adapter = adapter
        setListener(adapter, viewModel)

        viewModel.feed.observe(viewLifecycleOwner) { adapter.setFeeds(it) }
        viewModel.favFeed.observe(viewLifecycleOwner) { adapter.setFavItem(it) }
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        }

        viewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.loading.visibility = View.VISIBLE
            } else {
                binding.loading.visibility = View.GONE
            }
        })
        context?.let { viewModel.getFeed(it) }
    }

    //If needed later
    protected open fun setListener(adapter: FeedAdapter, viewModel: FeedViewModel) {
        val listener: (fav: Items) -> Unit = { fav ->
            context?.let {
                val newFavState = FavUtils.handleFavorite(it, fav.resource?.id)
                viewModel.updateItem(fav, newFavState)
            }
        }
        adapter.listener = listener
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
