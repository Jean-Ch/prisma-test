package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Feed (

  @SerializedName("apiVersion" ) var apiVersion : String? = null,
  @SerializedName("data"       ) var data       : Data?   = Data()

)