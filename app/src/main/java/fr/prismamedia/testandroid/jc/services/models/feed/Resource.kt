package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Resource (

  @SerializedName("path"      ) var path      : String? = null,
  @SerializedName("indexed"   ) var indexed   : String? = null,
  @SerializedName("index"     ) var index     : String? = null,
  @SerializedName("modified"  ) var modified  : String? = null,
  @SerializedName("id"        ) var id        : String? = null,
  @SerializedName("source"    ) var source    : String? = null,
  @SerializedName("published" ) var published : String? = null,
  @SerializedName("type"      ) var type      : String? = null,
  @SerializedName("url"       ) var url       : String? = null,
  @SerializedName("href"      ) var href      : String? = null

)