package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName


data class Category (

  @SerializedName("depth"          ) var depth          : Int?      = null,
  @SerializedName("resource"       ) var resource       : Resource? = Resource(),
  @SerializedName("descriptionSeo" ) var descriptionSeo : String?   = null,
  @SerializedName("title"          ) var title          : String?   = null,
  @SerializedName("titleSeo"       ) var titleSeo       : String?   = null

)