package fr.prismamedia.testandroid.jc.services.repositories
class MainRepository constructor(private val retrofitService: FeedService) {

    suspend fun getFeed() = retrofitService.getFeed()

}