package fr.prismamedia.testandroid.jc.services.models.feed

import com.google.gson.annotations.SerializedName

data class Authors (

    @SerializedName("firstname" ) var firstname : String? = null,
    @SerializedName("fullname"  ) var fullname  : String? = null,
    @SerializedName("email"     ) var email     : String? = null,
    @SerializedName("lastname"  ) var lastname  : String? = null,
    @SerializedName("username"  ) var username  : String? = null

)